# App Stacks

App stacks are preconfigured deployments for specialized clusters.

## TAP BUILD Cluster

Location: `stack/build`
Purpose: Tanzu Application Platform Build Profile Cluster

This stack presents the **BUILD** TAP profile in a multi-cluster build.

### Installation

#### In Phantom
This stack is automatically installed for clusters that declare it as their stack:

```
    - name: build
      spec:
        apps:
          valuesFile: true
          enabled: true
          git:
            url: https://gitlab.com/tanzuplatform/apps.git
            subPath: stack/build
            branch: main
```
#### Manually
```
kapp deploy -f <(ytt -f stack/build/manifests --data-values-file stack/build/values/) -a rv2-build-apps-ctrl -n kapp-controller -c
```

### Developer Onboarding

Teams onboard by committing first adding themselves to the list of onboarded teams under `onboarding` in: `stack/build/manifests/values.yaml`
And then adding their workloads to any location, for example: `stack/build/manifests/apps/others/spring-petclinic/workload.yaml`

The workload is then created in the build cluster.  Onboarding handles the necessary prerequisites (including namespace creation).

Additional Tekton Pipeline can also be included here, in the event they need to use something other than the default.  To leverage a different pipeline add the following label to the workload:
```
metadata:
  name: spring-petclinic
  namespace: others
  labels:
    apps.tanzu.vmware.com/language: somethingelse
```
And make sure that the same `somethingelse` is included on the Pipeline:
```
kind: Pipeline
metadata:
  name: a-different-pipeline
  namespace: sample
  labels:
    apps.tanzu.vmware.com/pipeline: test
    apps.tanzu.vmware.com/language: somethingelse
```

## TAP RUNTIME Cluster

Location: `stack/runtime`
Purpose: Tanzu Application Platform Run Profile Cluster

This stack presents the **RUN** TAP profile in a multi-cluster build.

### Installation

#### In Phantom
This stack is automatically installed for clusters that declare it as their stack:

```
    - name: runtime
      spec:
        apps:
          valuesFile: true
          enabled: true
          git:
            url: https://gitlab.com/tanzuplatform/apps.git
            subPath: stack/runtime
            branch: main
```
#### Manually
```
kapp deploy -f <(ytt -f stack/runtime/manifests --data-values-file stack/runtime/values/) -a rv2-runtime-apps-ctrl -n kapp-controller -c
```

### Developer Onboarding

**After a team has successfully built a workload it will trigger a merge request into this profile.**

The workload will not run until the team has similarly onboarded under `onboarding` in: `stack/runtime/manifests/values.yaml`.  This onboarding is different from onboarding to `build`, as it will create the necessary constructs in a production cluster.  Teams can be easily onboarded to build, but should pass the necessary validation of the team's ability to push to production before onboarding into runtime.