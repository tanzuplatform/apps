# TMC Self-Managed Install Overview

## Prerequisites (Done once)
1. Download the `tar` from the appropriate location.
1. Extract `tar xvf bundle-1.0.1-rc.2.tar`
1. Relocate the images:
    ```
    export HARBOR_PASSWORD=$(cat ~/.gcp-sa/tanzu-mission-control-registry.json )
    export HARBOR_USERNAME=_json_key
    ./tmc-sm push-images harbor --project=us-east5-docker.pkg.dev/pa-rvanvoorhees/tmc/self-managed-tmc
    ```
1. Upon success you will see the following:
    ```
    Image Staging Complete. Next Steps:
    Setup Kubeconfig (if not already done) to point to cluster:
    export KUBECONFIG={YOUR_KUBECONFIG}

    Create 'tmc-local' namespace: kubectl create namespace tmc-local

    Download Tanzu CLI from Customer Connect (If not already installed)

    Update TMC Self Managed Package Repository:
    Run: tanzu package repository add tanzu-mission-control-packages --url "us-east5-docker.pkg.dev/pa-rvanvoorhees/tmc/self-managed-tmc/package-repository:1.0.1-rc.2" --namespace tmc-local

    Create a values based on the TMC Self Managed Package Schema:
    View the Values Schema: tanzu package available get "tmc.tanzu.vmware.com/1.0.1-rc.2" --namespace tmc-local --values-schema
    Create a Values file named values.yaml matching the schema

    Install the TMC Self Managed Package:
    Run: tanzu package install tanzu-mission-control -p tmc.tanzu.vmware.com --version "1.0.1-rc.2" --values-file values.yaml --namespace tmc-local
    ```
1. **Note:** We do not need to run:
    ```
    kubectl create namespace tmc-local
    ```
    Creation of the namespace is located in `namespace.yaml`.
1. **Note:** We do not need to run:
    ```
    tanzu package repository add tanzu-mission-control-packages --url "us-east5-docker.pkg.dev/pa-rvanvoorhees/tmc/self-managed-tmc/package-repository:1.0.1-rc.2" --namespace tmc-local
    ```
    Creation of the package repository is in the `package-repository.yaml` file.
1. **Note:** Regarding:
    ```
    tanzu package available get "tmc.tanzu.vmware.com/1.0.1-rc.2" --namespace tmc-local --values-schema
    Create a Values file named values.yaml matching the schema
    ```
    Values are located in both the `schema.yaml` file and the `stack/tmc/values/ghost/tmc-secrets.ghost.sops.yaml` SOPS encrypted file.
1. **Note:**  We do not need to run:
    ```
    tanzu package install tanzu-mission-control -p tmc.tanzu.vmware.com --version "1.0.1-rc.2" --values-file values.yaml --namespace tmc-local
    ```
    The package install is done in the `tmc.yaml`, and the service account that package install would need is declared in `tmc-install-basics.yaml`

## Actual Install (done whenever values change)

If this is fully gitops:
1. Commit changes and they're run.

If this is not fully gitops:
1. After changes run this from the root of the repository:
```
kapp deploy -a tmc-system-apps.app -n kapp-controller -f <(sops exec-file --no-fifo stack/tmc/values/ghost/tmc-secrets.ghost.sops.yaml "ytt -f stack/tmc/manifests --data-values-file {}") -c
```
